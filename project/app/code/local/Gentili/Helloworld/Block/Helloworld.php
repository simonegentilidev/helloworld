<?php
/**
 *  Gentili Helloworld
 */

/**
 * class Gentili_Helloworld_Block_Helloworld
 *
 * Hello World Block.
 * @author Simone Gentili <simone.gentili@thinkopen.it>
 * @version 0.1.0.
 * @package Hello World.
 * @license GNU General Public License, version 3.
 */
class Gentili_Helloworld_Block_Helloworld extends Mage_Core_Block_Template
{

    /**
     * isEnabled
     *
     * Returns true if the module is enabled to be displayed
     * @return boolean
     */
    public function isEnabled()
    {
        return Mage::helper('gentili_helloworld')->isEnabled();
    }

    /**
     * getMessage
     *
     * Returns the custom message, if the module is enabled.
     * @return string|boolean
     */
    public function getMessage()
    {
        if ($this->isEnabled()) {
            return Mage::helper('gentili_helloworld')->getConfigData('configuration/custom_message');
        }

        return false;
    }

    /**
     * getHelloWorldUrl
     *
     * Return the Hello World URL
     * @return string
     */
    public function getHelloWorldUrl()
    {
        return Mage::getBaseUrl() . 'helloworld';
    }
}
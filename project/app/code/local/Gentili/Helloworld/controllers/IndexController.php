<?php
/**
 *  Gentili Helloworld
 */

/**
 * class Gentili_Helloworld_IndexController
 *
 * Main controller.
 * @author Simone Gentili <simone.gentili@thinkopen.it>
 * @version 0.1.0.
 * @package Hello World.
 * @license GNU General Public License, version 3.
 */
class Gentili_Helloworld_IndexController extends Mage_Core_Controller_Front_Action
{
    /**
     * indexAction
     */
    public function indexAction()
    {
        $this->loadLayout();
        $this->renderLayout();
    }
}